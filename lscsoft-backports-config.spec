Name:      lscsoft-backports-config
Version:   8
Release:   2%{?dist}
Summary:   Repository configuration for LSCSoft Backports

Group:     System Environment/Base
License:   GPLv3+
Source0:   lscsoft-backports.repo
BuildArch: noarch
Requires:  redhat-release >= %{version}
Requires:  lscsoft-production-config >= %{version}

%description
Repository configuration for LSCSoft Backports

%prep
%setup -q -c -T

%build

%install
rm -rf $RPM_BUILD_ROOT
install -dm 755 $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d
install -pm 644 %{SOURCE0} $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(0,root,root,-)
%config /etc/yum.repos.d/*

# dates should be formatted using: 'date +"%a %b %d %Y"'
%changelog
* Fri Jul 30 2021 Adam Mercer <adam.mercer@ligo.org> 8-2
- update URL for Rocky Linux

* Thu Jan 07 2021 Adam Mercer <adam.mercer@ligo.org> 8-1
- initial release
